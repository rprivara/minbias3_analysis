// ROOT macro for plotting eta closure in 1D and 2D
// Radek Privara, 08-2022

#include <iostream>
#include <string>

#include <TFile.h>
#include <TCanvas.h>
#include <THStack.h>
#include <TLegend.h>
#include <TPaveText.h>
#include <TStyle.h>
#include <TH1.h>
#include <TH2.h>

void TH2_subtract_one(TH2* h) {
    for (unsigned int x = 0; x < h->GetNbinsX(); x++) {
        for (unsigned int y = 0; y < h->GetNbinsY(); y++) {
            auto val = h->GetBinContent(x, y);
            h->SetBinError(x, y, 0);
            if (val == 0) return;
            h->SetBinContent(x, y, val-1);
        }
    }
}

void TH2_add_one(TH2* h) {
    for (unsigned int x = 0; x < h->GetNbinsX(); x++) {
        for (unsigned int y = 0; y < h->GetNbinsY(); y++) {
            auto val = h->GetBinContent(x, y);
            h->SetBinContent(x, y, val+1);
        }
    }
}

void TH1_nullify_errors(TH1* h) {
    for (unsigned int x = 0; x < h->GetNbinsX(); x++) {
        h->SetBinError(x, 0);
    }
}

void plot_closure_2D() {
    bool plot = true;
    bool debug = true;

    // Input file names
    std::string input_name_ND = "data/801451.Py8EG_A3_NNPDF23LO_minbias_ND.recon.AOD.e8486_s4122_r14418_r14420.root";
    std::string input_name_SD = "data/801452.Py8EG_A3_NNPDF23LO_minbias_SD.recon.AOD.e8486_s4122_r14418_r14420.root";
    std::string input_name_DD = "data/801453.Py8EG_A3_NNPDF23LO_minbias_DD.recon.AOD.e8486_s4122_r14418_r14420.root";
    auto input_file_ND = new TFile((input_name_ND).c_str(), "read");
    auto input_file_SD = new TFile((input_name_SD).c_str(), "read");
    auto input_file_DD = new TFile((input_name_DD).c_str(), "read");

    // Cross sections as weights for merging
    auto xs_ND = 58.96;
    auto xs_SD = 16.0;
    auto xs_DD = 4.0;
    auto frac_ND = 1;
    auto frac_SD = xs_SD/xs_ND;
    auto frac_DD = xs_DD/xs_ND;

    // Get numbers of events
    auto n_evt_ND = (dynamic_cast<TH1*>(input_file_ND->Get("hCutFlow_events")))->GetBinContent(1);
    auto n_evt_SD = (dynamic_cast<TH1*>(input_file_SD->Get("hCutFlow_events")))->GetBinContent(1);
    auto n_evt_DD = (dynamic_cast<TH1*>(input_file_DD->Get("hCutFlow_events")))->GetBinContent(1);

    // Scale factors to achieve target fraction
    auto scale_SD = (n_evt_ND / n_evt_SD) * frac_SD;
    auto scale_DD = (n_evt_ND / n_evt_DD) * frac_DD;

    if (debug) {
        std::cout << "  - Target ratio based on cross-sections (ND:SD:DD):  " << std::endl
                    << "        " << frac_ND << " : " << frac_SD << " : " << frac_DD << std::endl;

        std::cout << "  - Initial ratio (ND:SD:DD): " << std::endl
                    << "        events: " << n_evt_ND << " : " << n_evt_SD << " : " << n_evt_DD << std::endl
                    << "        events: 1 : " << n_evt_SD/n_evt_ND << " : " << n_evt_DD/n_evt_ND << std::endl;

        std::cout << "  - Scaling factors (ND:SD:DD): " << std::endl
                    << "        1" << " : " << scale_SD << " : " << scale_DD << std::endl;

        std::cout << "  - Ratio after scaling (ND:SD:DD): " << std::endl
                    << "        events: " << n_evt_ND << " : " << n_evt_SD * scale_SD << " : " << n_evt_DD * scale_DD << std::endl
                    << "        events: 1 : " << n_evt_SD/n_evt_ND * scale_SD << " : " << n_evt_DD/n_evt_ND * scale_DD << std::endl;
    }

    auto h_reco_2D_ND = (TH2*)input_file_ND->Get("Nominal/Ntrk1/Trk/trkPtEtaNW");
    auto h_reco_2D_SD = (TH2*)input_file_SD->Get("Nominal/Ntrk1/Trk/trkPtEtaNW");
    auto h_reco_2D_DD = (TH2*)input_file_DD->Get("Nominal/Ntrk1/Trk/trkPtEtaNW");
    auto h_truth_2D_ND = (TH2*)input_file_ND->Get("Nominal/All/Gen/genPtEtaPrim");
    auto h_truth_2D_SD = (TH2*)input_file_SD->Get("Nominal/All/Gen/genPtEtaPrim");
    auto h_truth_2D_DD = (TH2*)input_file_DD->Get("Nominal/All/Gen/genPtEtaPrim");
    if (h_reco_2D_ND == nullptr) std::cout << "nullptr1" << std::endl;
    if (h_reco_2D_SD == nullptr) std::cout << "nullptr2" << std::endl;
    if (h_reco_2D_DD == nullptr) std::cout << "nullptr3" << std::endl;
    if (h_truth_2D_ND == nullptr) std::cout << "nullptr4" << std::endl;
    if (h_truth_2D_SD == nullptr) std::cout << "nullptr5" << std::endl;
    if (h_truth_2D_DD == nullptr) std::cout << "nullptr6" << std::endl;

    // Scale with precalculated factors
    h_reco_2D_SD->Scale(scale_SD);
    h_reco_2D_DD->Scale(scale_DD);
    h_truth_2D_SD->Scale(scale_SD);
    h_truth_2D_DD->Scale(scale_DD);

    auto h_reco_2D = dynamic_cast<TH2*>(h_reco_2D_ND->Clone("Reco"));
    h_reco_2D->Add(h_reco_2D_SD);
    h_reco_2D->Add(h_reco_2D_DD);
    auto h_truth_2D = dynamic_cast<TH2*>(h_truth_2D_ND->Clone("Truth"));
    h_truth_2D->Add(h_truth_2D_SD);
    h_truth_2D->Add(h_truth_2D_DD);

    std::string corr_eff_path = "data/trk_eff.root";
    std::string corr_sec_path = "data/nonprim.root";
    std::string corr_okr_path = "data/okr.root";
    std::string corr_sb_path = "data/strange_baryon.root";
    std::string corr_fake_path = "data/fakes.root";

    auto corr_eff = new TFile(corr_eff_path.c_str());
    auto corr_sec = new TFile(corr_sec_path.c_str());
    auto corr_okr = new TFile(corr_okr_path.c_str());
    auto corr_sb = new TFile(corr_sb_path.c_str());
    auto corr_fake = new TFile(corr_fake_path.c_str());

    auto out = new TFile("data/out.root", "recreate");

    // Get correction maps and histograms
    // Efficiency
    auto h_eff_2D = (TH2D*)corr_eff->Get("trkEff");
    // Secondaries
    auto h_sec_2D = (TH2D*)corr_sec->Get("trkPtEtaSec");
    // OKR
    auto h_okr_2D = (TH2D*)corr_okr->Get("trkPtEtaPriOutFrac");
    // SB
    auto h_sb_2D = (TH2D*)corr_sb->Get("strange_baryon_frac");
    // Fakes
    auto h_fake_2D = (TH2D*)corr_fake->Get("fake_frac");

    // Scale by -1
    h_sec_2D->Scale(-1);
    h_okr_2D->Scale(-1);
    h_sb_2D->Scale(-1);
    h_fake_2D->Scale(-1);

    // Make intermediate subtracted histograms
    auto h_sub_sec = (TH2D*)h_sec_2D->Clone("Subtraction_Sec");
    auto h_sub_okr = (TH2D*)h_sub_sec->Clone("Subtraction_OKR");
    h_sub_okr->Add(h_okr_2D);
    auto h_sub_sb = (TH2D*)h_sub_okr->Clone("Subtraction_SB");
    h_sub_sb->Add(h_sb_2D);
    auto h_sub_fake = (TH2D*)h_sub_sb->Clone("Subtraction_Fake");
    h_sub_fake->Add(h_fake_2D);

    // Add unity to them
    TH2_add_one(h_sub_sec);
    TH2_add_one(h_sub_sb);
    TH2_add_one(h_sub_okr);
    TH2_add_one(h_sub_fake);

    // Apply individual correction steps
    // Efficiency
    auto h_corr1 = (TH2*)h_reco_2D->Clone("Corr1");
    h_corr1->Divide(h_eff_2D);
    // Secondaries
    auto h_corr2 = (TH2*)h_corr1->Clone("Corr2");
    h_corr2->Multiply(h_sub_sec);
    // OKR
    auto h_corr3 = (TH2*)h_corr1->Clone("Corr3");
    h_corr3->Multiply(h_sub_okr);
    // SB
    auto h_corr4 = (TH2*)h_corr1->Clone("Corr4");
    h_corr4->Multiply(h_sub_sb);
    // Fakes
    auto h_corr5 = (TH2*)h_corr1->Clone("Corr5");
    h_corr5->Multiply(h_sub_fake);


    // Make projections to eta
    auto p_corr1 = h_corr1->ProjectionY("Corr1_proj");
    auto p_corr2 = h_corr2->ProjectionY("Corr2_proj");
    auto p_corr3 = h_corr3->ProjectionY("Corr3_proj");
    auto p_corr4 = h_corr4->ProjectionY("Corr4_proj");
    auto p_corr5 = h_corr5->ProjectionY("Corr5_proj");
    auto p_truth = h_truth_2D->ProjectionY("Truth_proj");

    // Divide by truth to get ratios
    h_corr1->Divide(h_truth_2D);
    h_corr2->Divide(h_truth_2D);
    h_corr3->Divide(h_truth_2D);
    h_corr4->Divide(h_truth_2D);
    h_corr5->Divide(h_truth_2D);

    // Subtract unity to get ratio around 0
    TH2_subtract_one(h_corr1);
    TH2_subtract_one(h_corr2);
    TH2_subtract_one(h_corr3);
    TH2_subtract_one(h_corr4);
    TH2_subtract_one(h_corr5);

    // Divide projections by truth
    p_corr1->Divide(p_truth);
    p_corr2->Divide(p_truth);
    p_corr3->Divide(p_truth);
    p_corr4->Divide(p_truth);
    p_corr5->Divide(p_truth);
    p_truth->Divide(p_truth);

    TH1_nullify_errors(p_corr1);
    TH1_nullify_errors(p_corr2);
    TH1_nullify_errors(p_corr3);
    TH1_nullify_errors(p_corr4);
    TH1_nullify_errors(p_corr5);
    TH1_nullify_errors(p_truth);

    // Write histograms and projections to output file
    h_corr1->Write();
    p_corr1->Write();
    h_corr2->Write();
    p_corr2->Write();
    h_corr3->Write();
    p_corr3->Write();
    h_corr4->Write();
    p_corr4->Write();
    h_corr5->Write();
    p_corr5->Write();

    // Draw options
    p_truth->SetLineColor(1);
    p_corr1->SetLineColor(8);
    p_corr2->SetLineColor(4);
    p_corr3->SetLineColor(6);
    p_corr4->SetLineColor(2);
    p_corr5->SetLineColor(3);
    p_truth->SetMarkerColor(1);
    p_corr1->SetMarkerColor(8);
    p_corr2->SetMarkerColor(4);
    p_corr3->SetMarkerColor(6);
    p_corr4->SetMarkerColor(2);
    p_corr5->SetMarkerColor(3);
    unsigned int marker_style = 21;
    p_truth->SetMarkerStyle(marker_style);
    p_corr1->SetMarkerStyle(marker_style);
    p_corr2->SetMarkerStyle(marker_style);
    p_corr3->SetMarkerStyle(marker_style);
    p_corr4->SetMarkerStyle(marker_style);
    p_corr5->SetMarkerStyle(marker_style);
    float marker_size = 1.2;
    p_truth->SetMarkerSize(marker_size);
    p_corr1->SetMarkerSize(marker_size);
    p_corr2->SetMarkerSize(marker_size);
    p_corr3->SetMarkerSize(marker_size);
    p_corr4->SetMarkerSize(marker_size);
    p_corr5->SetMarkerSize(marker_size);
    float line_width = 2;
    p_truth->SetLineWidth(line_width);
    p_corr1->SetLineWidth(line_width);
    p_corr2->SetLineWidth(line_width);
    p_corr3->SetLineWidth(line_width);
    p_corr4->SetLineWidth(line_width);
    p_corr5->SetLineWidth(line_width);

    // Merge ratios into a stack
    auto stack = new THStack();
    stack->SetNameTitle("corr_vs_truth", "Corrected vs. truth;#eta;Corrected / Truth");
    stack->Add(p_truth);
    stack->Add(p_corr1);
    stack->Add(p_corr2);
    stack->Add(p_corr3);
    stack->Add(p_corr4);
    stack->Add(p_corr5);


    stack->Write();

    if (plot) {
        // Canvas
        auto canvas = new TCanvas("c", "c", 1600, 1200);

        // Draw options and draw
        stack->Draw("nostack,hist");
        stack->GetXaxis()->SetRangeUser(-2.5, 2.5);
        stack->SetMinimum(0.996);
        stack->SetMaximum(1.05);

        auto legend = new TLegend(0.5, 0.65, 0.8, 0.85);
        legend->SetBorderSize(0);
        legend->SetTextSize(0.04);
        legend->AddEntry(p_truth, "truth", "l");
        legend->AddEntry(p_corr1, "1/#varepsilon", "l");
        legend->AddEntry(p_corr2, "(1-f_{sec})/#varepsilon", "l");
        // legend->AddEntry(p_corr3, "1/#varepsilon(1-f_{sec}-f_{sb})", "l");
        legend->AddEntry(p_corr4, "(1-f_{sec}-f_{sb}-f_{okr})/#varepsilon", "l");
        legend->Draw();

        canvas->SaveAs("plots/corr_vs_truth.pdf");

        // Plot 2D histograms
        canvas->SetRightMargin(1.25);
        canvas->Update();
        gStyle->SetOptStat(0);
        canvas->SetLogx(1);

        float x_min = 0.4;
        float x_max = 200;
        float y_min = -2.7;
        float y_max = 2.7;
        float z_min = 0.7;
        float z_max = 1.3;

        gStyle->SetPalette(kThermometer);

        h_corr1->SetTitle("Corrected/Truth, corr = 1/#varepsilon; p_{T} [GeV];#eta");
        h_corr1->Draw("colz");
        h_corr1->GetXaxis()->SetRangeUser(x_min, x_max);
        h_corr1->GetYaxis()->SetRangeUser(y_min, y_max);
        h_corr1->GetZaxis()->SetRangeUser(z_min, z_max);
        canvas->SaveAs("plots/corr1.pdf");

        h_corr2->SetTitle("Corrected/Truth, corr = (1-f_{sec})/#varepsilon; p_{T} [GeV];#eta");
        h_corr2->Draw("colz");
        h_corr2->GetXaxis()->SetRangeUser(x_min, x_max);
        h_corr2->GetYaxis()->SetRangeUser(y_min, y_max);
        h_corr2->GetZaxis()->SetRangeUser(z_min, z_max);
        canvas->SaveAs("plots/corr2.pdf");

        h_corr3->SetTitle("Corrected/Truth, corr = (1-f_{sec}-f_{sb})/#varepsilon; p_{T} [GeV];#eta");
        h_corr3->Draw("colz");
        h_corr3->GetXaxis()->SetRangeUser(x_min, x_max);
        h_corr3->GetYaxis()->SetRangeUser(y_min, y_max);
        h_corr3->GetZaxis()->SetRangeUser(z_min, z_max);
        canvas->SaveAs("plots/corr3.pdf");

        h_corr4->SetTitle("Corrected/Truth, corr = (1-f_{sec}-f_{sb}-f_{okr})/#varepsilon; p_{T} [GeV];#eta");
        h_corr4->Draw("colz");
        h_corr4->GetXaxis()->SetRangeUser(x_min, x_max);
        h_corr4->GetYaxis()->SetRangeUser(y_min, y_max);
        h_corr4->GetZaxis()->SetRangeUser(z_min, z_max);
        canvas->SaveAs("plots/corr4.pdf");

        h_corr5->SetTitle("Corrected/Truth,  corr = (1-f_{sec}-f_{sb}-f_{okr})/#varepsilon; p_{T} [GeV];#eta");
        h_corr5->Draw("colz");
        h_corr5->GetXaxis()->SetRangeUser(x_min, x_max);
        h_corr5->GetYaxis()->SetRangeUser(y_min, y_max);
        h_corr5->GetZaxis()->SetRangeUser(z_min, z_max);
        canvas->SaveAs("plots/corr5.pdf");

        canvas->SetLogx(0);

        x_min = 0.4;
        x_max = 3;
        z_min = 0.95;
        z_max = 1.05;

        h_corr1->GetXaxis()->SetRangeUser(x_min, x_max);
        h_corr1->GetZaxis()->SetRangeUser(z_min, z_max);
        h_corr1->Draw("colz");
        canvas->SaveAs("plots/corr1_low.pdf");

        h_corr2->GetXaxis()->SetRangeUser(x_min, x_max);
        h_corr2->GetZaxis()->SetRangeUser(z_min, z_max);
        h_corr2->Draw("colz");
        canvas->SaveAs("plots/corr2_low.pdf");

        h_corr3->GetXaxis()->SetRangeUser(x_min, x_max);
        h_corr3->GetZaxis()->SetRangeUser(z_min, z_max);
        h_corr3->Draw("colz");
        canvas->SaveAs("plots/corr3_low.pdf");

        h_corr4->GetXaxis()->SetRangeUser(x_min, x_max);
        h_corr4->GetZaxis()->SetRangeUser(z_min, z_max);
        h_corr4->Draw("colz");
        canvas->SaveAs("plots/corr4_low.pdf");

        h_corr5->GetXaxis()->SetRangeUser(x_min, x_max);
        h_corr5->GetZaxis()->SetRangeUser(z_min, z_max);
        h_corr5->Draw("colz");
        canvas->SaveAs("plots/corr5_low.pdf");
    }
    out->Close();
}
