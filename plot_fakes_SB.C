// ROOT macro for plotting distributions for the Run3 MinBias analysis
// Radek Privara, 08-2022

#include <iostream>
#include <string>

#include <TFile.h>
#include <TCanvas.h>
#include <THStack.h>
#include <TLegend.h>
#include <TPaveText.h>
#include <TStyle.h>
#include <TLine.h>


void plot_fakes_SB() {
    // Input path
    auto orig_path = "data/mc_Pythia8A3-ND_100_fakeBad.root";
    auto new_path = "data/mc_Pythia8A3-ND_100_fakeOK.root";

    auto orig_file = new TFile(orig_path);
    auto new_file = new TFile(new_path);

    // Selection and systematics
    std::string selection = "Ntrk1";
    std::string systematics = "Nominal";

    auto h_fake = dynamic_cast<TH1*>(new_file->Get(TString(systematics + "/" + selection + "/Origin/trkPtFake")));
    auto h_SB = dynamic_cast<TH1*>(new_file->Get(TString(systematics + "/" + selection + "/Origin/trkPtSB")));
    auto h_fakeSB = dynamic_cast<TH1*>(orig_file->Get(TString(systematics + "/" + selection + "/Origin/trkPtFake")));

    // Output name and format
    std::string plot_format = ".pdf";

    // Prepare a canvas
    auto canvas = new TCanvas("canvas", "canvas", 1600, 1600);
    canvas->SetRightMargin(0.05);
    canvas->SetLeftMargin(0.1);
    canvas->SetLogx(1);
    gStyle->SetOptStat(0);
    // Grid
    canvas->SetGrid();
    gStyle->SetGridStyle(3);
    gStyle->SetGridColor(15);

    // Set unified maximum
    auto maximum = 2700.0;
    h_fake->SetMaximum(maximum);
    h_SB->SetMaximum(maximum);
    h_fakeSB->SetMaximum(maximum);

    // Draw options
    h_fake->GetXaxis()->SetTitleOffset(1.3);
    h_SB->GetXaxis()->SetTitleOffset(1.3);
    h_fakeSB->GetXaxis()->SetTitleOffset(1.3);
    auto width = 2.0;
    h_fake->SetLineWidth(width);
    h_SB->SetLineWidth(width);
    h_fakeSB->SetLineWidth(width);

    // Set titles
    h_fakeSB->SetTitle("Run2 fakes definition");
    h_fake->SetTitle("Run3 fakes definition");
    h_SB->SetTitle("Run3 SB definition");

    // 500MeV line
    auto line = new TLine(0.5, 0, 0.5, maximum);
    line->SetLineStyle(2);
    line->SetLineWidth(width);
    line->SetLineColor(2);

    // Legend
    auto legend = new TLegend(0.6, 0.75, 0.88, 0.88);
    legend->SetBorderSize(0);
    legend->SetTextSize(0.035);
    legend->AddEntry(h_fake, "Pythia8 A3 (ND)", "l");
    legend->AddEntry(line, "500 MeV", "l");

    h_fake->Draw("hist");
    line->Draw();
    legend->Draw();
    canvas->SaveAs(TString("plots/fakeTest_fakes" + plot_format));
    h_SB->Draw("hist");
    line->Draw();
    legend->Draw();
    canvas->SaveAs(TString("plots/fakeTest_SB" + plot_format));
    h_fakeSB->Draw("hist");
    line->Draw();
    legend->Draw();
    canvas->SaveAs(TString("plots/fakeTest_fakesSB" + plot_format));

    // Test differential
    auto h_test = dynamic_cast<TH1*>(h_fakeSB->Clone("subtraction"));
    h_fake->Scale(-1);
    h_SB->Scale(-1);
    h_test->Add(h_fake);
    h_test->Add(h_SB);
    h_test->Draw("hist");
    canvas->SaveAs(TString("plots/fakeTest_diff" + plot_format));
}
